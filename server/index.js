require("dotenv").config();
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");
const errorHandler = require("./handlers/error");
const productRoutes = require("./routes/product");
const authRoutes = require("./routes/auth");
const { loginRequired, ensureCorrectUser } = require("./middleware/auth");

const PORT = 8081;

app.use(cors());

app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
app.use(express.json());
app.use("/public", express.static("public"));
app.use("/api/auth", authRoutes);
app.use("/api", productRoutes);

app.use((req, res, next) => {
  let error = new Error("Not Found");
  error.code = 404;
  next(error);
});

app.use(errorHandler);

app.listen(PORT, () => {
  console.log(PORT);
});
