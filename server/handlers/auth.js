const db = require("../models");
const jwt = require("jsonwebtoken");

exports.signin = async function (request, response, next) {
  try {
    let user = await db.User.findOne({
      email: request.body.email,
    });

    let { id, username, profileImageUrl } = user;
    let isMatch = await user.comparePassword(request.body.password);
    if (isMatch) {
      let token = jwt.sign(
        {
          id,
          username,
          profileImageUrl,
        },
        process.env.SECRET_KEY,
        { expiresIn: 259200 }
      );
      return response.status(200).json({
        id,
        username,
        profileImageUrl,
        token,
      });
    } else {
      return next({
        status: 400,
        message: "Invalid Email/Password",
      });
    }
  } catch (error) {
    return next({
      status: 400,
      message: "Invalid Email/Password",
    });
  }
};

exports.signup = async function (request, response, next) {
  try {
    let user = await db.User.create(request.body);
    let { id, username, profileImageUrl } = user;
    let token = jwt.sign(
      {
        id,
        username,
        profileImageUrl,
      },
      process.env.SECRET_KEY
    );
    return response.status(200).json({
      id,
      username,
      profileImageUrl,
      token,
    });
  } catch (error) {
    if (error.code === 11000) {
      error.message = "Sorry, that username or email is taken";
    }

    return next({
      status: 400,
      message: error.message,
    });
  }
};
