const db = require("../models");

exports.product = async (req, res, next) => {
  const url = req.protocol + "://" + req.get("host");
  try {
    var productObj = {
      picture: url + "/public/" + req.file.filename,
      title: req.body.title,
      picUrl: req.body.picUrl,
      description: req.body.description,
      date: req.body.date,
      author: req.body.author,
      comments: req.body.comments,
    };
    let product = db.Product.create(productObj);
    res.status(200).json({ resolved: true });
  } catch (error) {
    return next({
      status: 400,
      message: error.message,
    });
  }
};

exports.createComment = async (req, res, next) => {
  try {
    let product = await db.Product.findByIdAndUpdate(
      req.params._id,
      {
        $push: {
          comments: [
            {
              name: req.body.name,
              date: req.body.date,
              comment: req.body.comment,
            },
          ],
        },
      },
      { new: true }
    );

    res.status(200).json(product);
  } catch (error) {
    return next({
      status: 400,
      message: error.message,
    });
  }
};

exports.findAllProducts = async (req, res, next) => {
  try {
    let product = await db.Product.find();
    res.status(200).json(product);
  } catch (error) {
    return next({
      status: 400,
      message: error.message,
    });
  }
};

exports.findProduct = async (req, res, next) => {
  try {
    let product = await db.Product.findById(req.params._id);
    res.status(200).json(product);
  } catch (error) {
    return next({
      status: 400,
      message: error.message,
    });
  }
};

exports.upvoteProductReview = async (req, res, next) => {
  try {
    let foundProduct = await db.Product.findById(req.params._id);
    foundProduct.upvotes.push(req.params.uID);
    await foundProduct.save();
    res.status(200).json(foundProduct.upvotes);
  } catch (error) {
    return next({
      status: 400,
      message: error.message,
    });
  }
};

exports.removeUpvoteProductReview = async (req, res, next) => {
  try {
    let foundProduct = await db.Product.findById(req.params._id);
    const newLikes = foundProduct.upvotes.filter(
      (upvote) => JSON.stringify(upvote) !== JSON.stringify(req.params.uID)
    );
    console.log(newLikes);
    foundProduct.upvotes = newLikes;
    await foundProduct.save();

    res.status(200).json(foundProduct.upvotes);
  } catch (error) {
    return next({
      status: 400,
      message: error.message,
    });
  }
};

exports.downvoteProductReview = async (req, res, next) => {
  try {
    let foundProduct = await db.Product.findById(req.params._id);
    foundProduct.downvotes.push(req.params.uID);
    await foundProduct.save();
    res.status(200).json(foundProduct.downvotes);
  } catch (error) {
    return next({
      status: 400,
      message: error.message,
    });
  }
};

exports.removeDownvoteProductReview = async (req, res, next) => {
  try {
    let foundProduct = await db.Product.findById(req.params._id);
    const newLikes = foundProduct.downvotes.filter(
      (downvote) => JSON.stringify(downvote) !== JSON.stringify(req.params.uID)
    );
    console.log(newLikes);
    foundProduct.downvotes = newLikes;
    await foundProduct.save();

    res.status(200).json(foundProduct.downvotes);
  } catch (error) {
    return next({
      status: 400,
      message: error.message,
    });
  }
};
