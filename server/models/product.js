const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
  picture: {
    type: String,
  },
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  picUrl: {
    type: String,
  },
  date: {
    type: String,
    required: true,
  },
  author: {
    type: String,
    required: true,
  },
  upvotes: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
  ],
  downvotes: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
  ],
  comments: [
    {
      name: { type: String },
      date: { type: String },
      comment: { type: String },
      upvotes: {
        type: Number,
      },
      downvotes: {
        type: Number,
      },
    },
  ],
});

productSchema.pre("remove", async function (next) {
  try {
    let user = await User.findById(this.userId);
    user.product.remove(this.id);
    await user.save();
    return next();
  } catch (error) {
    return next(error);
  }
});

const Product = mongoose.model("Product", productSchema);

module.exports = Product;
