const express = require("express");
const router = express.Router();
const {
  product,
  createComment,
  findAllProducts,
  findProduct,
  upvoteProductReview,
  removeUpvoteProductReview,
  downvoteProductReview,
  removeDownvoteProductReview,
} = require("../handlers/productHandler");
const db = require("../models");
const multer = require("multer");
var fs = require("fs");
var path = require("path");
const { v4: uuidv4 } = require("uuid");
const { loginRequired, ensureCorrectUser } = require("../middleware/auth");

const DIR = "./public/";

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, DIR);
  },
  filename: (req, file, cb) => {
    const fileName = file.originalname.toLowerCase().split(" ").join("-");
    cb(null, uuidv4() + "-" + fileName);
  },
});

var upload = multer({
  storage: storage,
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype == "image/png" ||
      file.mimetype == "image/jpg" ||
      file.mimetype == "image/jpeg" ||
      file.mimetype == "image/gif"
    ) {
      cb(null, true);
    } else {
      cb(null, false);
      return cb(new Error("Only .png, .jpg and .jpeg format allowed!"));
    }
  },
});

router
  .route("/product")
  .post(upload.single("picture"), product)
  .get(findAllProducts);
router
  .route("/product/:_id/upvote/:uID")
  .put(loginRequired, upvoteProductReview);
router
  .route("/product/:_id/upvote/:uID/remove")
  .put(loginRequired, removeUpvoteProductReview);
router.route("/product/:_id").get(findProduct);
router
  .route("/product/:_id/downvote/:uID")
  .put(loginRequired, downvoteProductReview);
router
  .route("/product/:_id/downvote/:uID/remove")
  .put(loginRequired, removeDownvoteProductReview);
router.route("/product/:_id/:uID").put(loginRequired, createComment);

module.exports = router;
