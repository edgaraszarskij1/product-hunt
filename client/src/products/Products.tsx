import React, {
  useState,
  Dispatch,
  SetStateAction,
  useEffect,
  useContext,
} from "react";
import Dialog from "../common/Dialog";
import { Product } from "../interfaces/ProductInterface";
import { ProductProps } from "../interfaces/ProductPropsInterface";
import { getAllProducts } from "../api/apiCalls";
import "./product.css";
import { Button } from "antd";
import { NavLink } from "react-router-dom";
import { ExpandAltOutlined } from "@ant-design/icons";
import { getProfile, loggedIn } from "../common/LoggedIn";
import { JwtPayload } from "jwt-decode";
import { AuthContext } from "../context/AuthContext";

const Products = (props: {
  setProductProps: Dispatch<SetStateAction<ProductProps>>;
}) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [product, setProduct] = useState<Product[]>([]);
  const [refresh, setRefresh] = useState<boolean>(false);
  const [profile, setProfile] = useState<JwtPayload>();
  const { isLoggedIn } = useContext(AuthContext);

  const handleOpen = () => {
    setIsOpen((prevState) => !prevState);
  };

  useEffect(() => {
    getAllProducts().then((data) => [setProduct(data)]);
  }, []);

  useEffect(() => {
    if (refresh) {
      getAllProducts().then((data) => [setProduct(data)]);
      setRefresh(false);
    }
  }, [refresh]);

  useEffect(() => {
    if (loggedIn() && profile === undefined) {
      setProfile(getProfile());
    }
  }, [profile]);

  return (
    <div className={"container"}>
      {product &&
        product.map((product) => (
          <div>
            <div key={product._id} className="inner-container">
              <img
                src={
                  typeof product.picture === "string"
                    ? product.picture
                    : undefined
                }
                alt="pic"
                className="picture"
              />
              <div className="title">{product.title}</div>
              <div className="author">
                &nbsp; {product.date}
                <div className="date">{product.author}</div>
              </div>
              <div className="description">
                {product.description.length > 249
                  ? product.description.slice(0, 250) + "..."
                  : product.description}
              </div>
              <NavLink to={`/product/${product._id}`} className={"not-working"}>
                {" "}
                <ExpandAltOutlined style={{ fontSize: 20 }} />
              </NavLink>
            </div>
          </div>
        ))}

      {isOpen && (
        <Dialog
          handleOpen={handleOpen}
          setProduct={setProduct}
          setRefresh={setRefresh}
          isOpen={isOpen}
        />
      )}
      <div className="button-pos">
        <Button
          type="primary"
          disabled={!isLoggedIn}
          block
          onClick={handleOpen}
        >
          Enter review
        </Button>
      </div>
    </div>
  );
};

export default Products;
