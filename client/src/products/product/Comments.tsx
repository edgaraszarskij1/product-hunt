import React, { useState } from "react";
import { Comment, Tooltip, Avatar } from "antd";
import { Comments } from "../../interfaces/ProductInterface";
import {
  DislikeOutlined,
  LikeOutlined,
  DislikeFilled,
  LikeFilled,
} from "@ant-design/icons";

const UserComments = (props: { comment: Comments }): JSX.Element => {
  const [likes, setLikes] = useState(0);
  const [dislikes, setDislikes] = useState(0);
  const [action, setAction] = useState(null);
  const actions = [
    <Tooltip key="comment-basic-like" title="Like">
      <span>
        {React.createElement(action === "liked" ? LikeFilled : LikeOutlined)}
        <span className="comment-action">{likes}</span>
      </span>
    </Tooltip>,
    <Tooltip key="comment-basic-dislike" title="Dislike">
      <span>
        {React.createElement(
          action === "disliked" ? DislikeFilled : DislikeOutlined
        )}
        <span className="comment-action">{dislikes}</span>
      </span>
    </Tooltip>,
    <span key="comment-basic-reply-to">Reply to</span>,
  ];

  return (
    <div key={props.comment._id}>
      <Comment
        actions={actions}
        author={props.comment.name}
        avatar={
          <Avatar
            src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
            alt="Han Solo"
          />
        }
        content={<p>{props.comment.comment}</p>}
        datetime={props.comment.date}
      />
    </div>
  );
};

export default UserComments;
