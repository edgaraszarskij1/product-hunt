import React, { useEffect, useState, useContext } from "react";
import { useParams } from "react-router-dom";
import { Product, Comments } from "../../interfaces/ProductInterface";
import { Profile } from "../../interfaces/UserInterface";
import {
  findProduct,
  addComment,
  addUpvoteOnReview,
  removeUpvoteOnReview,
  removeDownvoteOnReview,
  addDownvoteOnReview,
} from "../../api/apiCalls";
import { nanoid } from "nanoid";
import { ArrowLeftOutlined, LinkedinFilled } from "@ant-design/icons";
import { NavLink } from "react-router-dom";
import { Button } from "antd";
import {
  DislikeOutlined,
  LikeOutlined,
  DislikeFilled,
  LikeFilled,
} from "@ant-design/icons";
import UserComments from "./Comments";
import { getProfile, loggedIn } from "../../common/LoggedIn";
import "./comments.css";
import "./comments.less";
import { AuthContext } from "../../context/AuthContext";

import { JwtPayload } from "jwt-decode";
type idParams = {
  id: string;
};

const ProductInfo = () => {
  const [product, setProduct] = useState<Product>();
  const [isLoading, setIsLoading] = useState(false);
  const [profile, setProfile] = useState<Profile>();
  const [inputs, setInputs] = useState<Comments>({
    name: "",
    date: "",
    comment: "",
  });
  const { id } = useParams<idParams>();
  const { isLoggedIn } = useContext(AuthContext);
  useEffect(() => {
    setIsLoading(true);

    findProduct(id).then((data) => {
      setProduct(data);
      setIsLoading(false);
    });
  }, [id]);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInputs((prevState) => ({
      ...prevState,
      date: new Date().toLocaleDateString(),
      [event.target.name]: event.target.value,
    }));
  };
  useEffect(() => {
    if (loggedIn() && profile === undefined) {
      setProfile(getProfile());
    }
  }, [profile]);

  const onAddComment = (newComment: Comments) => {
    const { name, date, comment } = newComment;
    if (profile) {
      addComment(id, { name: name, date: date, comment: comment }, profile.id);
    }
    setProduct((prevState) => {
      if (prevState !== undefined) {
        return {
          ...prevState,
          comments: [
            ...prevState.comments,
            { _id: nanoid(), name: name, date: date, comment: comment },
          ],
        };
      }
      return;
    });

    setInputs({
      name: "",
      date: "",
      comment: "",
    });
  };

  const onUpvote = () => {
    if (profile && product) {
      if (product?.upvotes.includes(profile.id)) {
        const filteredItem = product.upvotes.filter(
          (upvote) => upvote !== profile.id
        );
        setProduct((prevState) => {
          if (prevState !== undefined && product) {
            return {
              ...prevState,
              upvotes: filteredItem,
            };
          }
        });
        removeUpvoteOnReview(id, profile.id);
      } else {
        setProduct((prevState) => {
          if (prevState !== undefined && product) {
            return {
              ...prevState,
              upvotes: [...prevState.upvotes, profile.id],
            };
          }
        });
        addUpvoteOnReview(id, profile.id);
      }
    }
  };

  const onDownvote = () => {
    if (profile && product) {
      if (product?.downvotes.includes(profile.id)) {
        const filteredItem = product.downvotes.filter(
          (downvote) => downvote !== profile.id
        );
        setProduct((prevState) => {
          if (prevState !== undefined && product) {
            return {
              ...prevState,
              downvotes: filteredItem,
            };
          }
        });
        removeDownvoteOnReview(id, profile.id);
      } else {
        setProduct((prevState) => {
          if (prevState !== undefined && product) {
            return {
              ...prevState,
              downvotes: [...prevState.downvotes, profile.id],
            };
          }
        });
        addDownvoteOnReview(id, profile.id);
      }
    }
  };

  return (
    <>
      <div className="container-comments">
        <NavLink to={`/`} className={"back-button"} style={{ zIndex: 4 }}>
          {" "}
          <ArrowLeftOutlined />
        </NavLink>
        <div className="comments-picture-container">
          <img
            alt=""
            src={
              typeof product?.picture === "string" ? product?.picture : "asd"
            }
            className="comments-picture"
          />
        </div>
        <div className="comments-body">
          <p className="comments-title">{product?.title}</p>
          <div className="comments-box">
            <p className="comments-author">{product?.author} &nbsp; </p>
            <p className="comments-date"> {product?.date}</p>
          </div>
          <div>{product?.description}</div>
        </div>
        {profile && product?.upvotes.includes(profile.id) ? (
          <LikeFilled onClick={onUpvote} className={"like-button"} />
        ) : (
          <LikeOutlined onClick={onUpvote} className={"like-button"} />
        )}
        {profile && product?.downvotes.includes(profile.id) ? (
          <DislikeFilled onClick={onDownvote} className={"dislike-button"} />
        ) : (
          <DislikeOutlined onClick={onDownvote} className={"dislike-button"} />
        )}
      </div>

      <div className="comments">
        <div className="input-fields-container ">
          <label className="label">Your Name: </label>
          <div className="input-box">
            <input
              id="name"
              name="name"
              className="comments-input"
              value={inputs.name}
              onChange={handleChange}
            />
          </div>
          <label className="label"> Comment: </label>
          <div className="input-box">
            <input
              id="comment"
              name="comment"
              className="comments-input"
              value={inputs.comment}
              onChange={handleChange}
            />
          </div>
        </div>
        <br />
        <Button
          onClick={() => onAddComment(inputs)}
          disabled={!isLoggedIn}
          type="primary"
          block
        >
          Post Comment
        </Button>

        {product?.comments &&
          product.comments.map((comment) => <UserComments comment={comment} />)}
      </div>
    </>
  );
};

export default ProductInfo;
