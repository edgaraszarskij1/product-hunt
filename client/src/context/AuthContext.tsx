import React, { createContext, useState, useEffect } from "react";
import { UserInfo } from "../interfaces/UserInterface";
import { setToken, loggedIn } from "../common/LoggedIn";
import { logout } from "../common/LoggedIn";
import { login } from "../api/auth";

export const AuthContext = createContext({
  isLoggedIn: false,
  onLogin: login,
  onLogout: logout,
});

const UserProvider = (props: { children: React.ReactNode }) => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const onLogin = async (userInfo: UserInfo) => {
    const response = await fetch("http://localhost:8081/api/auth/signin", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(userInfo),
    });

    if (!response.ok) {
      throw Error(response.statusText);
    }
    const data = await response.json();
    setIsLoggedIn(true);
    setToken(data.token);
  };

  const onLogout = () => {
    logout();
    setIsLoggedIn(false);
  };

  useEffect(() => {
    if (loggedIn()) {
      setIsLoggedIn(true);
    } else {
      setIsLoggedIn(false);
    }
  }, [isLoggedIn]);

  return (
    <AuthContext.Provider value={{ isLoggedIn, onLogin, onLogout }}>
      {props.children}
    </AuthContext.Provider>
  );
};

export default UserProvider;
