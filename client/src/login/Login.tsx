import React, { useState, useContext } from "react";
import Box from "../common/Box";
import { Button, Input } from "antd";
import { UserOutlined } from "@ant-design/icons";
import { UserInfo } from "../interfaces/UserInterface";
import { useHistory } from "react-router-dom";
import UserProvider, { AuthContext } from "../context/AuthContext";
import "./login.css";
import { login } from "../api/auth";

const Login = () => {
  let history = useHistory();
  const [inputData, setInputData] = useState<UserInfo>({
    email: "",
    password: "",
  });

  const onHandleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInputData((prevState) => ({
      ...prevState,
      [event.target.name]: event.target.value,
    }));
  };
  const { onLogin } = useContext(AuthContext);
  return (
    <Box>
      <Input
        size="large"
        name="email"
        placeholder="Email"
        prefix={<UserOutlined />}
        className="login-space"
        value={inputData.email}
        onChange={onHandleChange}
      />
      <Input.Password
        size="large"
        placeholder="Password"
        name="password"
        prefix={<UserOutlined />}
        className="login-space"
        value={inputData.password}
        onChange={onHandleChange}
      />
      <Button
        className="login-space"
        type="primary"
        onClick={() => onLogin(inputData).then((data) => history.push("/"))}
      >
        Login
      </Button>
    </Box>
  );
};

export default Login;
