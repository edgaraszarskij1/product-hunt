import { Dispatch, SetStateAction } from "react";
import { Product } from "./ProductInterface";

export interface ProductProps {
  products: Product[];
  setProduct: Dispatch<SetStateAction<Product[]>>;
}
