export interface Product {
  _id?: string;
  picture: string | File;
  title: string;
  description: string;
  picUrl: string;
  date: string;
  author: string;
  comments: Comments[];
  upvotes: string[];
  downvotes: string[];
}

export interface Comments {
  _id?: string;
  name: string;
  date: string;
  comment: string;
}
