export interface UserInfo {
  email: string;
  password: string;
}

export interface Profile {
  id: string;
  username: string;
  profileImageUrl: string;
  exp: number;
}
