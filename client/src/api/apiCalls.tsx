import { Product, Comments } from "../interfaces/ProductInterface";
import { loggedIn, getToken } from "../common/LoggedIn";
import { Headers } from "../interfaces/HeadersInterface";
const url = "http://localhost:8081/api/product/";
const header: Headers = {
  Accept: "application/json",
};

export const getAllProducts = async () => {
  const response = await fetch(url);
  if (!response.ok) {
    throw Error(response.statusText);
  }
  const data = await response.json();
  return data;
};

export const createNewProduct = async (newProduct: Product) => {
  const formData = new FormData();
  formData.append("picture", newProduct.picture);
  formData.append("picUrl", newProduct.picUrl);
  formData.append("title", newProduct.title);
  formData.append("description", newProduct.description);
  formData.append("author", newProduct.author);
  formData.append("date", newProduct.date);

  const response = await fetch(url, {
    method: "POST",
    body: formData,
  });
  if (!response.ok) {
    throw Error(response.statusText);
  }
  const data = await response.json();
  return data;
};

export const findProduct = async (id: string): Promise<Product> => {
  const response = await fetch(url + id);
  if (!response.ok) {
    throw Error(response.statusText);
  }
  const data = await response.json();
  return data;
};

export const addComment = async (
  id: string,
  comments: Comments,
  uID: string
) => {
  fetch(url + id + `/${uID}`, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: "Bearer " + getToken(),
    },

    body: JSON.stringify(comments),
  });
};

export const addUpvoteOnReview = async (id: string, uID: string) => {
  if (!loggedIn()) {
    header["Authorization"] = "Bearer " + getToken();
  }

  console.log(uID);
  const response = await fetch(url + id + "/upvote/" + uID, {
    method: "PUT",
    headers: {
      Accept: "application/json",

      Authorization: "Bearer " + getToken(),
    },
  });
  if (!response.ok) {
    throw Error(response.statusText);
  }
  const data = await response.json();
  return data;
};

export const removeUpvoteOnReview = async (id: string, uID: string) => {
  if (!loggedIn()) {
    header["Authorization"] = "Bearer " + getToken();
  }

  console.log(uID);
  const response = await fetch(url + id + "/upvote/" + uID + `/remove`, {
    method: "PUT",
    headers: {
      Accept: "application/json",

      Authorization: "Bearer " + getToken(),
    },
  });
  if (!response.ok) {
    throw Error(response.statusText);
  }
  const data = await response.json();
  return data;
};

export const addDownvoteOnReview = async (id: string, uID: string) => {
  if (!loggedIn()) {
    header["Authorization"] = "Bearer " + getToken();
  }

  const response = await fetch(url + id + "/downvote/" + uID, {
    method: "PUT",
    headers: {
      Accept: "application/json",

      Authorization: "Bearer " + getToken(),
    },
    body: JSON.stringify(uID),
  });
  if (!response.ok) {
    throw Error(response.statusText);
  }
  const data = await response.json();
  return data;
};

export const removeDownvoteOnReview = async (id: string, uID: string) => {
  if (!loggedIn()) {
    header["Authorization"] = "Bearer " + getToken();
  }

  const response = await fetch(url + id + "/downvote/" + uID + `/remove`, {
    method: "PUT",
    headers: {
      Accept: "application/json",

      Authorization: "Bearer " + getToken(),
    },
    body: JSON.stringify(uID),
  });
  if (!response.ok) {
    throw Error(response.statusText);
  }
  const data = await response.json();
  return data;
};
