import React from "react";
import "./box.css";
const Box = (props: { children: React.ReactNode }) => {
  return <div className="box-container">{props.children}</div>;
};

export default Box;
