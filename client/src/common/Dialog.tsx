import React, {
  useState,
  Dispatch,
  SetStateAction,
  HtmlHTMLAttributes,
} from "react";
import { Modal, Input } from "antd";
import { Product } from "../interfaces/ProductInterface";
import "./dialog.css";
import { createNewProduct } from "../api/apiCalls";

const Dialog = (props: {
  handleOpen: () => void;
  setProduct: Dispatch<SetStateAction<Product[]>>;
  setRefresh: Dispatch<SetStateAction<boolean>>;
  isOpen: boolean;
}) => {
  const [fileSelected, setFileSelected] = useState<File>();
  const [fileUrl, setFileUrl] = useState<string>("");
  const [resolved, setResolved] = useState<boolean>(false);
  const [inputs, setInputs] = useState<Product>({
    picture: "",
    picUrl: "",
    title: "",
    description: "",
    author: "",
    date: "",
    comments: [
      {
        name: "",
        date: "",
        comment: "",
      },
    ],
    upvotes: [""],
    downvotes: [""],
  });

  const { TextArea } = Input;

  const handleImageChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const fileList = event.target.files;
    setInputs((prevState) => ({
      ...prevState,
      [event.target.name]: event.target.value,
    }));
    if (!fileList) return;
    setFileUrl(URL.createObjectURL(fileList[0]));
    setFileSelected(fileList[0]);
    setInputs((prevState) => ({
      ...prevState,
      picture: fileList[0],
    }));
    console.log("loop in pictures");
  };

  const handleChange = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setInputs((prevState) => ({
      ...prevState,
      date: new Date().toLocaleDateString(),
      [event.target.name]: event.target.value,
    }));
    console.log(inputs);
  };

  const handleSubmit = (product: Product, event: React.SyntheticEvent) => {
    createNewProduct(product).then((response) => {
      if (response.resolved) {
        props.setRefresh(true);
        props.handleOpen();
      }
    });
  };

  return (
    <Modal
      visible={props.isOpen}
      width="50%"
      bodyStyle={{ height: 500 }}
      onOk={(event) => handleSubmit(inputs, event)}
      onCancel={props.handleOpen}
    >
      {fileSelected && <img alt="selected" src={fileUrl} className="img" />}{" "}
      <input
        id="picture"
        name="picture"
        type="file"
        multiple={false}
        onChange={handleImageChange}
      />
      <div style={{ paddingBottom: 20 }}>
        <Input
          placeholder="Title:"
          id="title"
          name="title"
          style={{ width: 300 }}
          defaultValue={inputs.title}
          onChange={handleChange}
        />
        <Input
          id="author"
          placeholder="First name:"
          style={{ width: 300 }}
          name="author"
          defaultValue={inputs.author}
          onChange={handleChange}
        />
      </div>
      <TextArea
        name="description"
        autoSize={{ minRows: 8, maxRows: 12 }}
        defaultValue={inputs.description}
        onChange={handleChange}
      />
    </Modal>
  );
};

export default Dialog;
