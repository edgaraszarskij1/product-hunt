import decode from "jwt-decode";
import { Profile } from "../interfaces/UserInterface";

export const setToken = (idToken: string) => {
  localStorage.setItem("jwt-token", idToken);
};

export const getToken = () => {
  return localStorage.getItem("jwt-token");
};

export const logout = () => {
  localStorage.removeItem("jwt-token");
};

export const loggedIn = () => {
  const token = getToken();

  return !!token && isTokenExpired(token);
};

export const isTokenExpired = (token: string) => {
  try {
    const decoded: Profile = decode(token);

    if (Date.now() >= decoded.exp * 1000) {
      return false;
    } else return true;
  } catch (err) {
    return false;
  }
};

export const getProfile = () => {
  let decoded = decode<Profile>(getToken() || "");
  return decoded;
};
