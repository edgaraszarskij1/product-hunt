import React, { useState, useContext } from "react";
import { Layout, Menu, Button } from "antd";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
} from "@ant-design/icons";
import "./layout.css";
import { NavLink } from "react-router-dom";
import { loggedIn, logout } from "../common/LoggedIn";
import UserProvider, { AuthContext } from "../context/AuthContext";

const { Header, Sider, Content } = Layout;

const MainLayout = (props: { children: React.ReactNode }): JSX.Element => {
  const [collapsed, setCollapsed] = useState<boolean>(true);

  const { isLoggedIn, onLogout } = useContext(AuthContext);

  const toggle = () => {
    setCollapsed((prevState) => !prevState);
  };

  return (
    <Layout>
      <Sider
        trigger={null}
        collapsible
        collapsed={collapsed}
        style={{
          overflow: "auto",
          height: "100vh",
          position: "sticky",
          top: 0,
          left: 0,
        }}
      >
        <div className="logo" />
        <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
          <Menu.Item key="1" icon={<UserOutlined />}>
            nav 1
          </Menu.Item>
          <Menu.Item key="2" icon={<VideoCameraOutlined />}>
            nav 2
          </Menu.Item>
          <Menu.Item key="3" icon={<UploadOutlined />}>
            nav 3
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            overflow: "auto",
            position: "sticky",
            top: 0,
            left: 0,
            padding: 0,
            zIndex: 1,
          }}
        >
          {React.createElement(
            collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
            {
              className: "trigger",
              onClick: toggle,
            }
          )}
          <div className="login-button">
            {!isLoggedIn ? (
              <>
                <NavLink to="/login">
                  <Button type="primary" style={{ marginRight: 24 }}>
                    Login
                  </Button>
                </NavLink>
                <NavLink to="/register">
                  <Button type="primary" style={{ marginRight: 24 }}>
                    Register
                  </Button>
                </NavLink>
              </>
            ) : (
              <>
                {" "}
                <NavLink to="/">
                  <Button
                    type="primary"
                    style={{ marginRight: 24 }}
                    onClick={() => onLogout()}
                  >
                    Logout
                  </Button>
                </NavLink>
              </>
            )}
          </div>
        </Header>

        <Content>{props.children}</Content>
      </Layout>
    </Layout>
  );
};

export default MainLayout;
