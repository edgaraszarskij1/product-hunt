import Products from "./products/Products";
import { Route, Switch } from "react-router-dom";
import ProductInfo from "./products/product/Product";
import Register from "./register/Register";
import Login from "./login/Login";

import "./App.css";
function App() {
  return (
    <div className="grid-container">
      <Switch>
        <Route path="/" exact component={Products} />
        <Route path="/product/:id" component={ProductInfo} />
        <Route path="/register" component={Register} />
        <Route path="/login" component={Login} />
      </Switch>
    </div>
  );
}

export default App;
